[Adblock Plus 2.0]
! Title: .Outbrain Default filters
! Expires: 1 days
!:partner_token=Outbrain
###ob_ad
##.abp_ob_exist
#@#a[data-obtrack^="http://paid.outbrain.com/network/redir?"]
#@#a[data-oburl^="https://paid.outbrain.com/network/redir?"]
#@#a[data-redirect^="https://paid.outbrain.com/network/redir?"]
#@#a[data-url^="http://paid.outbrain.com/network/redir?"] + .author
#@#a[data-widget-outbrain-redirect^="http://paid.outbrain.com/network/redir?"]
#@#a[href^="https://paid.outbrain.com/network/redir?"]
#@#a[onmousedown^="this.href='http://paid.outbrain.com/network/redir?"][target="_blank"]
#@#a[onmousedown^="this.href='http://paid.outbrain.com/network/redir?"][target="_blank"] + .ob_source
#@#a[onmousedown^="this.href='https://paid.outbrain.com/network/redir?"][target="_blank"]
#@#a[onmousedown^="this.href='https://paid.outbrain.com/network/redir?"][target="_blank"] + .ob_source
#@#a[target="_blank"][onmousedown="this.href^='http://paid.outbrain.com/network/redir?"]
#@#a[href^="https://ad.doubleclick.net/"]
#@#.item-container-ad
#@#a[href^="https://bs.serving-sys.com"]
#@#a[href^="https://track.adform.net/"]
#@#.js-outbrain-container
#@#.OUTBRAIN
#@#a[href^="https://plarium.com/"]
#@#a[href*="/www/delivery/"]
#@#.ob-widget-header
#@#.ob-p.ob-dynamic-rec-container
#@##outbrain_widget_0
#@#.ob-smartfeed-wrapper
#@#.outbrain
@@||widgets.outbrain.com^$third-party
@@||outbrain.com/outbrain.js
@@||outbrain.com/utils/*&adblck=true
@@||paid.outbrain.com/network/redir?$popup
@@||marketing.outbrain.com/network/redir?$popup
@@||serving-sys.com/BurstingPipe/adServer.bs?$popup
@@||doubleclick.net/ddm/$popup
@@||zemanta.com^
@@||h.lqm.io^$image
@@||widgets.outbrain.com/nanoWidget/externals/obPixelFrame/obPixelFrame.htm$subdocument,document
! Global
@@||widgets.outbrain.com/widgetOBUserSync/obUserSync.html$document,subdocument
||outbrain.com/widget/detect/px.gif?ch=1
@@||outbrain.com/widget/detect/px.gif?ch=2

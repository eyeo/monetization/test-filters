[Adblock Plus 2.0]
! Title: .Optima Net Default filters
! Expires: 1 days
!
@@||servg1.net/px.gif?ch=1
||servg1.net/px.gif?ch=2
@@||servg1.net/?uid=*ab=1$script
@@||servg1.net/banner/*$image
@@||servg1.net/o.js^$script
@@||ads.projectagoraservices.com/*$script,xmlhttprequest,document
@@||projectagora-adtag-library.com/*$script,xmlhttprequest,document
@@||projectagora-*adomik.com/*$image
@@||projectagora-d.openx.net/*$xmlhttprequest

[Adblock Plus 2.0]
! Title: .Taboola Default filters
! Expires: 1 days
!:partner_token=Taboola
#@#.trc-content-sponsoredUB
#@#.trc-content-sponsored
#@#a[href^="https://bs.serving-sys.com"]
#@#.trc_rbox_div
#@#.trc_rbox
#@#.trc_rbox_border_elm
#@#a[href^="https://ad.doubleclick.net/"]
#@#.trc_related_container
#@#a[href^="https://track.adform.net/"]
#@#a[href^="https://plarium.com/"]
##.bx-creative a[target="_blank"][href^="http://api.taboola.com/"]
##.banner_ad
##.sponsored_ad
#@##taboola-below-article-thumbnails
#@##taboola-right-rail-thumbnails
#@#.trc_excludable
#@#.trc-first-recommendation
#@#.trc-spotlight-first-recommendation
#@#.trc_spotlight_item
#@#.trc_related_container div[data-item-syndicated="true"]
@@||taboola.com^$script
@@||images.*.criteo.net/img/img?c=*&u=$image
@@||pix.*.criteo.net/img/img?c=*&u=$image
@@||images.taboola.com/taboola/image/fetch/$image
@@&publisherID=$popup
@@||doubleclick.net/ddm/$popup
@@||track.adform.net^$popup
@@||ads.youniversalmedia.com^$popup
@@||plarium.com^*&adPixel=taboola_$popup
@@||serving-sys.com/BurstingPipe/adServer.bs?$popup
@@^utm_source=taboola^$popup
@@^utm_source=taboola_$popup
@@||pips.taboola.com^
